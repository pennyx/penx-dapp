import { ByteVector, contract } from '@waves/ts-contract'

export interface myContract {
  foo(value: String | Number | Boolean | ByteVector)
}

export const { foo } = contract<myContract>()('3MvoqrxFuBbRY4NvaEHQ65U4KNZU4nqwJa6')
